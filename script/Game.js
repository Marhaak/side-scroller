var SideScroller = SideScroller || {};

SideScroller.Game = function() {}

SideScroller.Game.prototype = {

	preload: function() {

		this.game.time.advancedTiming = true;
	}, 

	create: function() {

		this.map = this.game.add.tilemap('level1');

		// The first parameter is the tileset we made in "Tiled", 
		// the second is the key to the tilesheet asset in Phaser we loaded in "Preload.js"
		this.map.addTilesetImage('tiles_spritesheet', 'gameTiles');

		// 'backgroundLayer' is the layer we named in "Tiled".
		this.backgroundlayer = this.map.createLayer('backgroundLayer');

		this.blockedLayer = this.map.createLayer('blockedLayer');

		// Turing on collision for the blockedLayer.
		this.map.setCollisionBetween(0, 100, true, 'blockedLayer', false);

		this.backgroundLayer.resizeWorld();

		// Create the player.
		this.player = this.game.add.sprite(100, 300, 'player');
	},

	update: function() {

	},

	render: function() {
		this.game.debug.text(this.game.time.fps || '--', 20, 70, "#00ff00", "40pxCourier");
	}
};