var SideScroller = SideScroller || {};

SideScroller.Boot = function(){};

// Setting game config. and loading the assets for the loading screen.

SideScroller.Boot.prototype  = {
	preload: function() {

		// Assets we'll use in the loading screen.

		this.load.image('preloadbar', 'assets/images/preloader-bar.png');
	},

	create: function() {

		// Loading screen will have a white background.
		this.game.stage.backgroundColor = '#fff';

		// Scaling options.
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

		// Have the game sentered horizontally.
		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;

		// Screensize will be set automatically.
		this.scale.setScreenSize = true;

		// What physics system to use.
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.state.start('Preload');
	}
}